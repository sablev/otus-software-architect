# ADR-002: Использовать GitLab

## Контекст

Университет расширил свой курс по компьютерным наукам и хочет автоматизировать оценку простых заданий по программированию. Принято решение для автоматизации проверки и оценки отправляемых студентами решений развернуть на вычислительных мощностях университета готовое решение.

## Варианты

### GitLab

Для автоматизации проверки и оценки отправляемых студентами решений на вычислительных мощностях университета развернуть GitLab CE ([https://gitlab.com/browse/gitlab-ce](https://gitlab.com/browse/gitlab-ce)).

Анализ:

* Реализация функциональности не вызывает вопросов
* Доступность, производительность и масштабируемость могут быть обеспечены:
  * готовое решение будет выбрано с учётом требований к атрибутам качества
* Время поставки будет небольшим:
  * потребуется установка и настройка серверов для развёртывания решения
  * потребуются развёртывание и настройка готового решения
* Удобство использования может быть обеспечено:
  * GitLab привычен для большинства разработчиков ПО
* Стоимость решения будет средней:
  * затраты на приобретение, установку, настройку и эксплуатацию серверов для развёртывания решения
  * затраты на развёртывание и настройку готового решения

### Gitea

Для автоматизации проверки и оценки отправляемых студентами решений на вычислительных мощностях университета развернуть Gitea Server ([https://about.gitea.com/products/gitea](https://about.gitea.com/products/gitea)) и Gitea Runner ([https://about.gitea.com/products/runner](https://about.gitea.com/products/runner)).

* Реализация функциональности не вызывает вопросов
* Доступность, производительность и масштабируемость могут быть обеспечены:
  * готовое решение будет выбрано с учётом требований к атрибутам качества
* Время поставки будет небольшим:
  * потребуется установка и настройка серверов для развёртывания решения
  * потребуются развёртывание и настройка готового решения
* Удобство использования может быть обеспечено:
  * Gitea не привычен для большинства разработчиков ПО
* Стоимость решения будет средней:
  * затраты на приобретение, установку, настройку и эксплуатацию серверов для развёртывания решения
  * затраты на развёртывание и настройку готового решения

## Сравнительный анализ

Сравнительный анализ вариантов на предмет возможности реализации функциональности и достижимости приоритетных характеристик.

Возможность реализации функциональности и достижимость каждой характеристики оценена по 5-бальной шкале:

* 1 — характеристика недостижима,
* 5 — характеристика достижима с высокой вероятностью.

|                    | 1. Развернуть GitLab | 2. Развернуть Gitea |
| :----------------- | :------------------: | :-----------------: |
| Функциональность   |          5           |          4          |
| Доступность        |          5           |          5          |
| Производительность |          5           |          5          |
| Масштабируемость   |          5           |          5          |
| Надёжность         |          5           |          5          |
| Юзабилити          |          5           |          5          |
| Время поставки     |          5           |          5          |
| Стоимость решения  |          5           |          5          |

## Решение

Для автоматизации проверки и оценки отправляемых студентами решений принято решение использовать GitLab CE.